from matplotlib import pyplot as plt
import numpy as np

def main():
    data = np.genfromtxt('time.csv',delimiter=',')
    plt.figure(1)
    color = ['b','g','c']
    title = ['Macbook pro(2.6 GHz x 2)','Galileo cluster (2 nodes with 400 MHz x 1 each)','Galileo(400 MHz x 1)']
    for i in range(data.shape[1]-1):
        if i in [0,1]: 
            plt.figure(i)
        plt.plot(data[:,0],data[:,i+1],color[i]+'o-',label=title[i])
        if i==0:
            plt.title(title[i])
        else:
            plt.title('Galileo comparison')
        plt.xlabel('No. of processes')
        plt.ylabel('Time(s)')
    plt.legend()
    plt.show()



if __name__=='__main__':
    main()
